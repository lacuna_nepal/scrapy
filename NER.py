import pycountry
import spacy
from spacy import displacy
import en_core_web_sm
nlp = en_core_web_sm.load()

def find_countries_in_article(text):    
    doc = nlp(text)
    a = [(X.text, X.label_) for X in doc.ents]
    countries = list(set([x for x,y in a if y=="GPE"]))
    countries = ",".join(countries)
    county = []
    for country in pycountry.countries:
        if country.name in countries:
            county.append(country.name)
    county = ",".join(county)
    return county
 
    
    
    
def find_stakeholders_from_articles(text):
    doc = nlp(text)
    a = [(X.text, X.label_) for X in doc.ents]
    stakeholders = list(set([x for x,y in a if y=="ORG"]))
    stakeholders = ",".join(stakeholders)
    return stakeholders
        
    